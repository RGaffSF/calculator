package com.galvanize;

import java.util.Optional;

public class Application {


    public Integer calculator(String input) throws Exception {

        if (input.equals("")) return 0;
        String delimiter = ",";

        if (input.startsWith("//")) {
            int lineBreakPosition = (input.indexOf("\n"));
            System.out.println(lineBreakPosition);
            delimiter = input.substring(2,lineBreakPosition);
            System.out.println(delimiter);
            input = input.substring(lineBreakPosition+1);
        }

        String myInput = input.replaceAll("\\n|;|:|,|\\s",delimiter);
        String csv[] = myInput.split(delimiter);

        int calc = Integer.parseInt(csv[0]);
        String errmsg = "";

        for (int i=0; i<csv.length; i++) {

                if (Integer.parseInt(csv[i]) < 0) {
                    errmsg += (csv[i] + ",");
                    System.out.println(errmsg);
                } else if (Integer.parseInt(csv[i]) <= 1000){
                    calc += Integer.parseInt(csv[i]);
                }

        } if (errmsg.length() > 0) {
            StringBuilder throwError = new StringBuilder(errmsg);
            throwError.deleteCharAt(errmsg.lastIndexOf(","));
            System.out.println(throwError);
            throw new IllegalArgumentException("negatives not allowed " + throwError);
        }

        return calc - Integer.parseInt(csv[0]);
    }

}

