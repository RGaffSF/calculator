package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ApplicationTest {

//@Test
//public void emptyString() {
//Application application = new Application();
//assertEquals("",application.calculator(""));
//}

//@Test
//public void returnsInput() {
//Application application = new Application();
//assertEquals("1,2",application.calculator("1,2"));
//}

@Test
public void returnsAnInteger() throws Exception {
Application application = new Application();
assertEquals(3,application.calculator("3"));
    }

@Test
    public void returnsZeroForEmpty() throws Exception {
    Application application = new Application();
    assertEquals(0, application.calculator(""));
}

@Test
    public void addTwoNumbers() throws Exception {
    Application application = new Application();
    assertEquals(3,application.calculator("1,2"));
}

@Test public void addFourNumbers() throws Exception {
    Application application = new Application();
    assertEquals(10,application.calculator("1,2,3,4"));
}

@Test
    public void addLineBreaks() throws Exception {
    Application application = new Application();
    assertEquals(6, application.calculator("1,2\n3"));
}

@Test
    public void differentDelimiters() throws Exception {
    Application application = new Application();
    assertEquals(10,application.calculator("1;2,3\n4"));
}

@Test
    public void moreDifferentDelimiters() throws Exception {
        Application application = new Application();
        assertEquals(14,application.calculator("1;2,3\n4:2 2"));
    }

@Test
    public void pickedDelimiter() throws Exception {
    Application application = new Application();
    assertEquals(10, application.calculator("//#\n1#2#3#4"));
}

@Test
    public void pickedMessedUpDelimiter() throws Exception {
    Application application = new Application();
    assertEquals(12, application.calculator("//#\n1#2:3,4#2"));
    }


@Test
    public void throwsAnError() {
    Application application = new Application();
    IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
            ()->application.calculator("-1,2"));
    assertTrue(thrown.getMessage().contains("negatives not allowed -1"));
}

    @Test
    public void throwsBiggerError() {
        Application application = new Application();
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class,
                ()->application.calculator("-1,2,-3,-8"));
        assertTrue(thrown.getMessage().contains("negatives not allowed -1,-3,-8"));
    }

    @Test
    public void ignoreBigNumbers() throws Exception {
        Application application = new Application();
        assertEquals(9, application.calculator("//#\n1#2:3000,4#2"));
    }

    @Test
    public void multipleDigitDelimiters() throws Exception {
        Application application = new Application();
        assertEquals(9, application.calculator("//##\n1##2:3000,4##2"));
    }

    @Test
    public void multiplesDigitDelimiters() throws Exception {
        Application application = new Application();
        assertEquals(9, application.calculator("//####\n1####2:3000,4####2"));
    }

    @Test
    public void multiplesDigitDelimiters2() throws Exception {
        Application application = new Application();
        assertEquals(9, application.calculator("//---\n1---2:3000,4---2"));
    }


//@Test
//    public void testing() throws Exception {
//    Application application = new Application();
//    application.calculator("-1,2,-3,-4");
//}
}
